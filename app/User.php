<?php

namespace App;

use App\Models\Api\v1\Cliente;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {
	use HasApiTokens, Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */ 
	protected $fillable = [
		'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * Retorna los datos extras del cliente.
	 * @return [type] [description]
	 */
	public function cliente() {
		return $this->hasOne(Cliente::class, 'users_id');
	}
}
