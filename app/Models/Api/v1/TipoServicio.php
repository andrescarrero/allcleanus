<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class TipoServicio extends Model {

	protected $table    = "tipos_servicios";
	protected $fillable = ['id', 'tipo_servicio'];

	/**
	 * Retorna los servicios disponibles para ese tipo de servicio
	 * @return [type] [description]
	 */
	public function servicio() {
		return $this->hasMany(Servicio::class, 'tipos_servicios_id');
	}

	/**
	 * Retorna las promociones que tiene dicho tipo de servicio.
	 * @return [type] [description]
	 */
	public function promocionesServicio() {
		return $this->hasMany(PromocionServicio::class, 'tipos_servicios_id');
	}
}
