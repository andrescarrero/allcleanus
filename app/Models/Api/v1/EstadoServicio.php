<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class EstadoServicio extends Model {

	protected $table    = "estados_servicios";
	protected $fillable = ['id', 'nombre', 'descripcion'];

	/**
	 * Retorna los servicios prestados con determinado estado de servicio
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->hasMany(ServicioPrestado::class, 'estados_servicios_id');
	}

}
