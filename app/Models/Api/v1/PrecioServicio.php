<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\Atributo;
use App\Models\Api\v1\Especificacion;
use App\Models\Api\v1\Servicio;
use App\Models\Api\v1\ServicioPrestado;
use Illuminate\Database\Eloquent\Model;

class PrecioServicio extends Model {
	protected $table = "precios_servicios";

	protected $fillable = ['id', 'precio', 'atributos_id', 'especificaciones_id', 'servicios_id', 'precios_servicios_id'];

	/**
	 * Retorna el atributo asociado al precio del servicio
	 * @return [type] [description]
	 */
	public function atributo() {
		return $this->belongsTo(Atributo::class, 'id');
	}

	/**
	 * Retorna la especificacion asociada al precio del servicio
	 * @return [type] [description]
	 */
	public function especificacion() {
		return $this->belongsTo(Especificacion::class, 'id');
	}

	/**
	 * Retorna el servicio asociado al precio del servicio
	 * @return [type] [description]
	 */
	public function servicios() {
		return $this->belongsTo(Servicio::class, 'id');
	}

	/**
	 * Retorna el precio de servicio prestado.
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->belongsTo(ServicioPrestado::class, 'id');
	}
}
