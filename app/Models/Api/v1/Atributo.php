<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\PrecioServicio;
use Illuminate\Database\Eloquent\Model;

class Atributo extends Model {
	protected $table    = "atributos";
	protected $fillable = ['id', 'descripcion'];

	/**
	 * Retorna los precios que hay para ese atributo
	 * @return [type] [description]
	 */
	public function precioServicio() {
		return $this->hasMany(PrecioServicio::class, 'atributos_id');
	}
}
