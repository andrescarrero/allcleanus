<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\Cupon;
use App\Models\Api\v1\Descuento;
use App\Models\Api\v1\PrecioServicio;
use Illuminate\Database\Eloquent\Model;

class ServicioPrestado extends Model {

	protected $table    = "servicios_prestados";
	protected $fillable = ['id', 'observacion_servicio', 'fecha_inicio', 'fecha fin', 'hora_inicio', 'hora_fin', 'fecha_hora_inicio_cliente', 'fecha_hora_fin_cliente', 'ubicacion', 'cantidad', 'tiempo_estimado_llegada', 'duracion_servicio', 'clientes_id', 'estados_servicios_id', 'pagos_servicios_id', 'calificaciones_trabajadores_id', 'calificaciones_servicios_id', 'cupones_id', 'servicios_trabajadores_id', 'descuentos_id', 'precios_servicios_id'];

	/**
	 * Retorna el pago de servicio asociado al servicio prestado
	 * @return [type] [description]
	 */
	public function pagoServicio() {
		return $this->hasOne(PagoServicio::class, 'id');
	}

	/**
	 * Retorna la calificación del servicio asociada al servicio prestado
	 * @return [type] [description]
	 */
	public function calificacionServicio() {
		return $this->hasOne(CalificacionServicio::class, 'id');
	}

	/**
	 * Retorna la calificación del trabajador asociada al servicio prestado
	 * @return [type] [description]
	 */
	public function calificacionTrabajador() {
		return $this->hasOne(CalificacionTrabajador::class, 'id');
	}

	/**
	 * Retorna el cliente que contrató el servicio prestado
	 * @return [type] [description]
	 */
	public function cliente() {
		return $this->belongsTo(Cliente::class, 'id');
	}

	/**
	 * Retorna el estado del servicio en el que se encuentra el servicio prestado
	 * @return [type] [description]
	 */
	public function estadoServicio() {
		return $this->belongsTo(EstadoServicio::class, 'id');
	}

	/**
	 * Retorna el servicio prestado por el trabajador
	 * @return [type] [description]
	 */
	public function servicioTrabajador() {
		return $this->belongsTo(ServicioTrabajador::class, 'id');
	}

	/**
	 * Retorna el cupon que se aplicó al servicio prestado
	 * @return [type] [description]
	 */
	public function cupon() {
		return $this->belongsTo(Cupon::class, 'id');
	}

	/**
	 * Retorna el descuento que se aplicó al servicio prestado
	 * @return [type] [description]
	 */
	public function descuento() {
		return $this->belongsTo(Descuento::class, 'id');
	}

	/**
	 * Retorna el tipo y precio del servicio prestado
	 * @return [type] [description]
	 */
	public function precioServicio() {
		return $this->hasOne(PrecioServicio::class, 'id');
	}
}
