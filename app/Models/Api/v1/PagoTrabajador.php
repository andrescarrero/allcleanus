<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class PagoTrabajador extends Model {

	protected $table    = "pagos_trabajadores";
	protected $fillable = ['id', 'fecha_pago', 'monto_pago', 'estado', 'trabajadores_id'];

	/**
	 * Retorna el trabajador al que pertence dicho pago
	 * @return [type] [description]
	 */
	public function trabajador() {
		return $this->belongsTo(Trabajador::class, 'id');
	}

}
