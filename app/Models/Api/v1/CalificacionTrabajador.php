<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class CalificacionTrabajador extends Model {

	protected $table    = "calificaciones_trabajadores";
	protected $fillable = ['id', 'calificacion', 'comentario'];

	/**
	 * Retorna el servicio prestado por el que el trabajador recibió la calificación
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->belongsTo(ServicioPrestado::class, 'id');
	}

}
