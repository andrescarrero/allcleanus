<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class CalificacionServicio extends Model {

	protected $table    = "calificaciones_servicios";
	protected $fillable = ['id', 'calificacion', 'otro_trabajador', 'comentario'];

	/**
	 * Retorna el servicio prestado para la calificación en cuestión (cuanto no va el trabajador que corresponde y se califica el servicio y no al trabajador)
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->belongsTo(ServicioPrestado::class, 'id');
	}

}
