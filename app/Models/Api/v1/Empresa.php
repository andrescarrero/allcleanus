<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\Trabajador;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {
	protected $table    = "empresas";
	protected $fillable = ['id', 'nombre', 'telefono', 'telefono_2', 'direccion'];

	/**
	 * Retorna los trabajadores que pertenecen a dicha empresa.
	 * @return [type] [description]
	 */
	public function trabajadores() {
		return $this->hasMany(Trabajador::class, 'empresas_id');
	}
}
