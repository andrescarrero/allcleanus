<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class ServicioTrabajador extends Model {

	protected $table    = "servicios_trabajadores";
	protected $fillable = ['id', 'trabajadores_id', 'servicios_id'];

	/**
	 * Retorna los servicios prestados por un trabajador y servicio
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->hasMany(ServicioPrestado::class, 'servicios_trabajadores_id');
	}

	/**
	 * Retorna el trabajador que presta ese servicio
	 * @return [type] [description]
	 */
	public function trabajador() {
		return $this->belongsTo(Trabajador::class, 'id');
	}

	/**
	 * Retorna el Servicio que presta un trabajador
	 * @return [type] [description]
	 */
	public function servicio() {
		return $this->belongsTo(Servicio::class, 'id');
	}

}
