<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\TipoDescuento;
use Illuminate\Database\Eloquent\Model;

class Descuento extends Model {
	protected $table    = "descuentos";
	protected $fillable = ['id', 'clave', 'cantidad_descuentos', 'fecha_desde', 'fecha_hasta', 'monto_minimo', 'monto_maximo', 'descuento_porcentaje', 'descuento_monto', 'tipos_descuentos_id'];

	/**
	 * Retorna el tipo de descuento al cual pertenece
	 * @return [type] [description]
	 */
	public function tipoDescuento() {
		return $this->belongsTo(TipoDescuento::class, 'id');
	}

	/**
	 * Retorna las promosiones que tienen dichos servicios
	 * @return [type] [description]
	 */
	public function promosionesServicios() {
		return $this->hasMany(PromocionServicio::class, 'descuentos_id');
	}

	/**
	 * Retorna los servicios contratados por un cliente
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->hasMany(ServicioPrestado::class, 'descuentos_id');
	}
}
