<?php

namespace App\Models\Api\v1;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

	protected $table    = "clientes";
	protected $fillable = ['id', 'identificacion', 'tipodocumento', 'nombre', 'apellido', 
	// 'correo', 'contrasena', 
	'ubicacion', 
	// 'token', 
	'cuenta_bancaria', 'fecha_registro', 'roles_id','users_id', 'telefono', 'telefono_2'];

	/**
	 * Retorna el rol que posee un cliente
	 * @return [type] [description]
	 */
	public function rol() {
		return $this->belongsTo(Rol::class, 'id');
	}

	/**
	 * Retorna el usuario del que se está mostrando la data.
	 * @return [type] [description]
	 */
	public function usuario() {
		return $this->belongsTo(User::class, 'id');
	}

	/**
	 * Retorna los servicios contratados por un cliente
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->hasMany(ServicioPrestado::class, 'clientes_id');
	}

}
