<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class PromocionServicio extends Model {

	protected $table    = "promociones_servicios";
	protected $fillable = ['id', 'cupones_id', 'tipos_servicios_id', 'servicios_id', 'descuentos_id'];

	/**
	 * Retorna el cupon que es aplicable a dicho servicio
	 * @return [type] [description]
	 */
	public function cupon() {
		return $this->belongsTo(Cupon::class, 'id');
	}

	/**
	 * Retorna el servicio al cual se le aplica el cupon
	 * @return [type] [description]
	 */
	public function servicio() {
		return $this->belongsTo(Servicio::class, 'id');
	}

	/**
	 * Retorna el tipo de servicio al cual se le aplica el cupon
	 * @return [type] [description]
	 */
	public function cuponServicio() {
		return $this->belongsTo(TipoServicio::class, 'id');
	}

	/**
	 * Retorna el tipo de servicio al cual se le aplica el cupon
	 * @return [type] [description]
	 */
	public function descuento() {
		return $this->belongsTo(Descuento::class, 'id');
	}

}
