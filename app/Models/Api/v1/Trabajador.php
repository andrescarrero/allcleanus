<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model {

	protected $table    = "trabajadores";
	protected $fillable = ['id', 'identificacion', 'tipodocumento', 'nombre', 'apellido', 'correo', 'contrasena', 'cuenta_bancaria', 'condicion', 'fecha_ingreso', 'fecha_retiro', 'token', 'foto', 'monto_acumulado', 'telefono', 'telefono_2','empresas_id'];

	/**
	 * Retorna los pagos que se les ha hecho a un trabajador
	 * @return [type] [description]
	 */
	public function pagoTrabajador() {
		return $this->hasMany(PagoTrabajador::class, 'trabajadores_id');
	}

	/**
	 * Retorna los servicios que puede prestar un trabajador.
	 * @return [type] [description]
	 */
	public function servicioTrabajador() {
		return $this->hasMany(ServicioTrabajador::class, 'trabajadores_id');
	}

	/**
	 * Retorna la empresa a la que pertenece el trabajador
	 * @return [type] [description]
	 */
	public function empresa() {
		return $this->belongsTo(Empresa::class, 'id');
	}

}
