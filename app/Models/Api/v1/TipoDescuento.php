<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class TipoDescuento extends Model {
	protected $table    = "tipos_descuentos";
	protected $fillable = ['id', 'tipo_descuento', 'descripcion'];

	/**
	 * Retorna los descuentos de ese tipo de descuento
	 * @return [type] [description]
	 */
	public function descuento() {
		return $this->hasMany(Descuento::class, 'tipos_descuentos_id');
	}
}
