<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model {

	protected $table    = "roles";
	protected $fillable = ['id', 'nombre'];

	/**
	 * Retorna los clientes que tienen cierto Rol
	 * @return [type] [description]
	 */
	public function clientes() {
		return $this->hasMany(Cliente::class, 'roles_id');
	}
}
