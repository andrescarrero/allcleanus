<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class TipoCupon extends Model {

	protected $table    = "tipos_cupones";
	protected $fillable = ['id', 'tipo_cupon', 'descripcion'];

	/**
	 * Retorna los cupones de ese tipo de cupon
	 * @return [type] [description]
	 */
	public function cupon() {
		return $this->hasMany(Cupon::class, 'tipos_cupones_id');
	}
}
