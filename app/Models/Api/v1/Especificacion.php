<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\PrecioServicio;
use Illuminate\Database\Eloquent\Model;

class Especificacion extends Model
{
    protected $table    = "especificaciones";
	protected $fillable = ['id', 'descripcion'];

	/**
	 * Retorna los precios que hay para esa especificaciones
	 * @return [type] [description]
	 */
	public function precioServicio() {
		return $this->hasMany(PrecioServicio::class, 'especificaciones_id');
	}
}
