<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class PagoServicio extends Model {

	protected $table    = "pagos_servicios";
	protected $fillable = ['id', 'pago', 'devolucion', 'recargo', 'estado'];

	/**
	 * Retorna el servicio prestado al cual pertenece dicho pago.
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->belongsTo(ServicioPrestado::class, 'id');
	}

}
