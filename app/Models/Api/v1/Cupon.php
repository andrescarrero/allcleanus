<?php

namespace App\Models\Api\v1;

use Illuminate\Database\Eloquent\Model;

class Cupon extends Model {

	protected $table    = "cupones";
	protected $fillable = ['id', 'clave', 'cantidad_cupones', 'fecha_desde', 'fecha_hasta', 'descuento_porcentaje', 'descuento_monto','tipos_cupones_id'];

	/**
	 * Retorna los servicios prestados con ese número de cupon
	 * @return [type] [description]
	 */
	public function servicioPrestado() {
		return $this->hasMany(ServicioPrestado::class, 'cupones_id');
	}

	/**
	 * Retorna el tipo de cupon, al que pertenece el cupon en cuestion
	 * @return [type] [description]
	 */
	public function tipoCupon() {
		return $this->belongsTo(TipoCupon::class, 'id');
	}

	/**
	 * Retorna los servicios para los que ha sido creado el descuento
	 * @return [type] [description]
	 */
	public function promosionServicio() {
		return $this->hasMany(PromocionServicio::class, 'cupones_id');
	}

}
