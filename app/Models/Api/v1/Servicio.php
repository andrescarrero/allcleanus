<?php

namespace App\Models\Api\v1;

use App\Models\Api\v1\CuponServicio;
use App\Models\Api\v1\PrecioServicio;
use App\Models\Api\v1\ServicioTrabajador;
use App\Models\Api\v1\TipoServicio;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model {

	protected $table    = "servicios";
	protected $fillable = ['id', 'servicio', 'porc_trabajador', 'porc_tercero', 'tipos_servicios_id'];
	// 'horas', 'costo'

	/**
	 * Retorna los servicios que puede prestar un trabajador
	 * @return [type] [description]
	 */
	public function serviciosTrabajadores() {
		return $this->hasMany(ServicioTrabajador::class, 'servicios_id');
	}

	/**
	 * Retorna el tipo de servicio al que pertence dicho Servicio
	 * @return [type] [description]
	 */
	public function tipoServicio() {
		return $this->belongsTo(TipoServicio::class, 'id');
	}

	/**
	 * Retorna las promociones aplicables a dicho servicio.
	 * @return [type] [description]
	 */
	public function promocionesServicos() {
		return $this->hasMany(PromocionServicio::class, 'servicios_id');
	}

	/**
	 * Retorna los diferentes precios de dicho servicio
	 * @return [type] [description]
	 */
	public function preciosServicios() {
		return $this->hasMany(PrecioServicio::class, 'servicios_id');
	}
}
