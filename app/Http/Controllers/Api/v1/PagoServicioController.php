<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\PagoServicio;
use Illuminate\Http\Request;

class PagoServicioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\PagoServicio  $pagoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function show(PagoServicio $pagoServicio) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\PagoServicio  $pagoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, PagoServicio $pagoServicio) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\PagoServicio  $pagoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(PagoServicio $pagoServicio) {
		//
	}
}
