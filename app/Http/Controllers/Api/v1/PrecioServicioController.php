<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\PrecioServicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PrecioServicioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\PrecioServicio  $precioServicio
	 * @return \Illuminate\Http\Response
	 */
	public function show(PrecioServicio $precioServicio) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\PrecioServicio  $precioServicio
	 * @return \Illuminate\Http\Response
	 */
	public function edit(PrecioServicio $precioServicio) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\PrecioServicio  $precioServicio
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, PrecioServicio $precioServicio) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\PrecioServicio  $precioServicio
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(PrecioServicio $precioServicio) {
		//
	}

	/**
	 * Función para consultar los precios del servicio de Home Cleaning
	 * @return [type] [description]
	 */
	public function consultarPrecioHomeCleaning() {

		try {

			//Se conuslta el padre, el que posee los precios
			$PrecioServicio = PrecioServicio::select('id', 'precio')
				->where('servicios_id', 1)
				->where('precios_servicios_id', null)
				->get();

			//Se consulta el hijo, el que tiene la cantidad de camas
			$HijosBedServicio = PrecioServicio::select('precios_servicios.id', 'precios_servicios.precio', 'precios_servicios.precios_servicios_id', 'esp.descripcion')
				->join('especificaciones as esp', 'precios_servicios.especificaciones_id', 'esp.id')
				->where('servicios_id', 1)
				->where('atributos_id', 1) //Bed
				->where('precios_servicios_id', '<>', null)
				->get();

			//Se consulta el hijo, el que tiene la cantidad de baños
			$HijosBathServicio = PrecioServicio::select('precios_servicios.id', 'precios_servicios.precio', 'precios_servicios.precios_servicios_id', 'esp.descripcion')
				->join('especificaciones as esp', 'precios_servicios.especificaciones_id', 'esp.id')
				->where('servicios_id', 1)
				->where('atributos_id', 2) //Bath
				->where('precios_servicios_id', '<>', null)
				->get();

			//Se crea el Array auxiliar que retornará los valores unificados
			$precios = [];

			//Ciclo por cantidad de servicios
			foreach ($PrecioServicio as $key => $value) {

				//Ciclo para los hijos
				foreach ($HijosBedServicio as $key => $valueBed) {

					//Si el hijo es de ese padre, se compaginan, precio con camas y baños
					if ($value->id == $valueBed->precios_servicios_id) {
						$precios[] = array(
							'id'     => $value->id,
							'bed'    => intval($valueBed['descripcion']),
							'bath'   => intval($HijosBathServicio[$key]['descripcion']),
							'precio' => $value->precio,
						);
					}

				}
			}

			return response()->json(["PrecioServicio" => $precios], 200);

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el precio del servicio {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response("Ha ocurrido un problema al tratar de consultar el precio del servicio", 500);
		}
	}

	/**
	 * Función para consultar los precios del servicio de Office Cleaning
	 * @return [type] [description]
	 */
	public function consultarPrecioOfficeCleaning() {

		try {

			//Se hace la consulta del servicio para conocer los precios
			$PrecioServicio = PrecioServicio::select('precios_servicios.id', 'precios_servicios.precio', 'esp.descripcion')
				->join('especificaciones as esp', 'precios_servicios.especificaciones_id', 'esp.id')
				->where('precios_servicios.servicios_id', 4) //Office cleaning
				->get();

			return response()->json(["PrecioServicio" => $PrecioServicio], 200);

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el precio del servicio {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response("Ha ocurrido un problema al tratar de consultar el precio del servicio", 500);
		}
	}

	/**
	 * Función para consultar los precios del servicio de Garage Cleaning
	 * @return [type] [description]
	 */
	public function consultarPrecioGarageCleaning() {

		try {

			//Se hace la consulta del servicio para conocer los precios
			$PrecioServicio = PrecioServicio::select('precios_servicios.id', 'precios_servicios.precio', 'esp.descripcion')
				->join('especificaciones as esp', 'precios_servicios.especificaciones_id', 'esp.id')
				->where('precios_servicios.servicios_id', 3) //Garage cleaning
				->get();

			return response()->json(["PrecioServicio" => $PrecioServicio], 200);

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el precio del servicio {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response("Ha ocurrido un problema al tratar de consultar el precio del servicio", 500);
		}
	}

	/**
	 * Función para consultar los precios del servicio de Painting Service
	 * @return [type] [description]
	 */
	public function consultarPrecioPaintingService() {

		try {

			//Se hace la consulta del servicio para conocer los precios, en este caso el valor del precio se debe multiplicar por la cantidad de SQF proporcionados por el usuario
			$PrecioServicio = PrecioServicio::select('precios_servicios.id', 'precios_servicios.precio', 'esp.descripcion')
				->join('especificaciones as esp', 'precios_servicios.especificaciones_id', 'esp.id')
				->where('precios_servicios.servicios_id', 6) //Painting Service
				->get();

			return response()->json(["PrecioServicio" => $PrecioServicio], 200);

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el precio del servicio {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response("Ha ocurrido un problema al tratar de consultar el precio del servicio", 500);
		}
	}

	/**
	 * Función para consultar los precios del servicio de Painting Front Door
	 * @return [type] [description]
	 */
	public function consultarPrecioPaintingFrontDoor() {

		try {

			//Se hace la consulta del servicio para conocer los precios, en este caso el valor del precio se debe multiplicar por la cantidad proporcionada por el usuario
			$PrecioServicio = PrecioServicio::select('precios_servicios.id', 'precios_servicios.precio', 'esp.descripcion')
				->join('especificaciones as esp', 'precios_servicios.especificaciones_id', 'esp.id')
				->where('precios_servicios.servicios_id', 8) //Painting Service
				->get();

			return response()->json(["PrecioServicio" => $PrecioServicio], 200);

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el precio del servicio {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response("Ha ocurrido un problema al tratar de consultar el precio del servicio", 500);
		}
	}

}