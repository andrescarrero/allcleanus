<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Empresa  $Empresa
	 * @return \Illuminate\Http\Response
	 */
	public function show(Empresa $Empresa) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Empresa  $Empresa
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Empresa $Empresa) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Empresa  $Empresa
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Empresa $Empresa) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Empresa  $Empresa
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Empresa $Empresa) {
		//
	}
}
