<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\Descuento;
use App\Models\Api\v1\PromocionServicio;
use App\Models\Api\v1\Servicio;
use App\Models\Api\v1\TipoDescuento;
use App\Models\Api\v1\TipoServicio;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class DescuentoController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Descuento  $descuento
	 * @return \Illuminate\Http\Response
	 */
	public function show(Descuento $descuento) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Descuento  $descuento
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Descuento $descuento) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Descuento  $descuento
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Descuento $descuento) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Descuento  $descuento
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Descuento $descuento) {
		//
	}

	/**
	 * Método para consultar si existe descuento aplicable para dicho servicio, en la fecha indicada
	 * @param  Request $request [date][cupon_code][service]
	 * @return [type]           [description]
	 */
	public function consultarDescuento(Request $request) {
		try {

			//Fecha del servicio
			$fechaServicio = "";

			//Fecha en la que solicitó el servicio
			if (strpos($request['date'], ':') !== false) {
				$fechaServicio = Carbon::createFromFormat('m-d-Y H:i:s', $request['date']);

			} else {
				$fechaServicio = Carbon::createFromFormat('m-d-Y', $request['date']);
			}

			//Se consulta primero los descuentos Generales
			$descuento = $this->buscarDescuento($request['service'], 1, $fechaServicio, $request['monto']);

			//Si no se encuntra ninguno, se consultan descuentos por tipo de servicio
			if ($descuento == null) {
				$descuento = $this->buscarDescuento($request['service'], 2, $fechaServicio, $request['monto']);

				//Si no se encuntra ninguno, se consultan descuentos por servicio específico
				if ($descuento == null) {
					$descuento = $this->buscarDescuento($request['service'], 3, $fechaServicio, $request['monto']);
				}
			}

			//Opción de retorno
			if ($descuento != null) {
				//se retorna el cupon encontrado
				return response()->json(["Discount" => $descuento], 200);
			} else {
				//se retorna la alerta
				return response()->json(["Alerta" => "This service does not have a current discount"], 200);
			}

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el descuento {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response()->json(["Error" => "Error en comunicacion"], 500);
		}
	}

	/**
	 * Método para buscar un descuento
	 * @param  [type] $servicio           [Id Servicio]
	 * @param  [type] $consultarDescuento [Tipo de descuento a consultar "G" 1, "S" 2, "I" 3]
	 * @param  [type] $fechaServicio      [Fecha dado el servicio]
	 * @return [type]                     [description]
	 */
	public function buscarDescuento($servicio, $consultarDescuento, $fechaServicio, $monto) {

		//Se crea variable para retornar, si es true, devolverá el descuento encontrado, si es False, un msj de alerta
		$retornar = true;

		//Buscamos que exista un descuento "G" general para la fecha.
		$descuento = Descuento::where('tipos_descuentos_id', $consultarDescuento)
			->whereDate('fecha_desde', "<=", $fechaServicio->toDateString())
			->where('cantidad_descuentos', '>', 0)
			->first();

		//Comprobar si encaja dentro del monto
		//Si ambos son diferentes a null, el monto debe estar entre ambos.
		if ($descuento['monto_maximo'] != null && $descuento['monto_minimo'] != null) {
			if ($monto < $descuento['monto_minimo'] || $monto > $descuento['monto_maximo']) {
				$descuento = null;
			}
		} else if ($descuento['monto_minimo'] != null) {
			//si es menor al monto del descuento, no aplica.
			if ($monto < $descuento['monto_minimo']) {
				$descuento = null;
			}
		} else if ($descuento['monto_maximo'] != null) {
			//si es mayor al monto del descuento, no aplica.
			if ($monto > $descuento['monto_maximo']) {
				$descuento = null;
			}
		}

		//Si existe un descuento
		if ($descuento != null) {
			//En caso de que fecha_hasta sea diferente de null, se compara con la fecha del servicio
			if ($descuento['fecha_hasta'] != null) {
				$fecha_hasta = Carbon::createFromFormat('Y-m-d', $descuento['fecha_hasta']);
				//La fecha hasta desde ser menor o igual a la fecha de contrato del servicio
				if ($fechaServicio->lessThanOrEqualTo($fecha_hasta) == false) {
					$retornar  = false;
					$descuento = null;
				}
			}

			//Existe aún un descuento seleccionado.
			if ($retornar) {
				//Se busca el tipo de descuento
				$tipoDescuento = TipoDescuento::where('id', $descuento['tipos_descuentos_id'])->first();
				//Buscar si está afiliado a una promosión
				$promocion = PromocionServicio::where('descuentos_id', $descuento['id'])->first();
				//Se busca el tipo de servicio del servicio donde se está buscando el descuento
				$tipoServicio = TipoServicio::find(Servicio::find($servicio)['tipos_servicios_id']);

				//Existe una promosión asociada a este servicio.
				if ($promocion != null) {
					//Si es por tipo, aplica para el tipo del servicio en cuestión?
					if ($tipoDescuento['tipo_descuento'] == "S" && $promocion['tipos_servicios_id'] != $tipoServicio['id']) {
						//El descuento es para otro tipo de servicio, no para el actual
						$descuento = null;
					}
					//Si es individual, aplica para este servicio?
					if ($tipoDescuento['tipo_descuento'] == "I" && $promocion['servicios_id'] != $servicio) {
						//El descuento es para otro servicio en específico, no para el actual
						$descuento = null;
					}
				} else {
					//No existe promoción asociada
					$descuento = null;
				}
			}
		}

		return $descuento;
	}
}
