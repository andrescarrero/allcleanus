<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\PagoTrabajador;
use Illuminate\Http\Request;

class PagoTrabajadorController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\PagoTrabajador  $pagoTrabajador
	 * @return \Illuminate\Http\Response
	 */
	public function show(PagoTrabajador $pagoTrabajador) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\PagoTrabajador  $pagoTrabajador
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, PagoTrabajador $pagoTrabajador) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\PagoTrabajador  $pagoTrabajador
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(PagoTrabajador $pagoTrabajador) {
		//
	}
}
