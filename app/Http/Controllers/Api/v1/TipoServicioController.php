<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\TipoServicio;
use Illuminate\Http\Request;

class TipoServicioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\TipoServicio  $tipoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function show(TipoServicio $tipoServicio) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\TipoServicio  $tipoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, TipoServicio $tipoServicio) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\TipoServicio  $tipoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TipoServicio $tipoServicio) {
		//
	}
}
