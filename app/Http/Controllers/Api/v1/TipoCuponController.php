<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\TipoCupon;
use Illuminate\Http\Request;

class TipoCuponController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\TipoCupon  $tipoCupon
	 * @return \Illuminate\Http\Response
	 */
	public function show(TipoCupon $tipoCupon) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\TipoCupon  $tipoCupon
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, TipoCupon $tipoCupon) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\TipoCupon  $tipoCupon
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TipoCupon $tipoCupon) {
		//
	}
}
