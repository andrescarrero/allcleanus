<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\CalificacionServicio;
use Illuminate\Http\Request;

class CalificacionServicioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\CalificacionServicio  $calificacionServicio
	 * @return \Illuminate\Http\Response
	 */
	public function show(CalificacionServicio $calificacionServicio) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\CalificacionServicio  $calificacionServicio
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CalificacionServicio $calificacionServicio) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\CalificacionServicio  $calificacionServicio
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CalificacionServicio $calificacionServicio) {
		//
	}
}
