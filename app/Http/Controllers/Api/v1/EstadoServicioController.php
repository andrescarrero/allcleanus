<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\EstadoServicio;
use Illuminate\Http\Request;

class EstadoServicioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\EstadoServicio  $estadoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function show(EstadoServicio $estadoServicio) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\EstadoServicio  $estadoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, EstadoServicio $estadoServicio) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\EstadoServicio  $estadoServicio
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(EstadoServicio $estadoServicio) {
		//
	}
}
