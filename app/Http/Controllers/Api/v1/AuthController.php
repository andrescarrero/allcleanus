<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\Cliente;
use App\Models\Api\v1\PagoServicio;
use App\Models\Api\v1\Rol;
use App\Models\Api\v1\ServicioPrestado;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

	/**
	 * Registro básico de un usuario, solo correo y contraseña
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function register(Request $request) {
		$validator = Validator::make($request->all(), [
			// 'name'             => 'required',
			'email'            => 'required|email',
			'password'         => 'required',
			'confirm_password' => 'required|same:password',
		]);

		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 422);
		}

		$input             = $request->all();
		$input['password'] = bcrypt($request->get('password'));
		$user              = User::create($input);
		$token             = $user->createToken('AllCleanUsPersonal')->accessToken;

		return response()->json([
			'token' => $token,
			'user'  => $user,
		], 200);
	}

	/**
	 * Hacer login con el usuario para pedir credenciales, al responder con token, ese toquen es q se pide para seguir consultando la API
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function login(Request $request) {

		if (Auth::attempt($request->only('email', 'password'))) {
			$user  = Auth::user();
			$token = $user->createToken('AllCleanUsPersonal')->accessToken;
			return response()->json([
				'token' => $token,
				'user'  => $user,
			], 200);
		} else {
			return response()->json(['error' => 'Unauthorised'], 401);
		}
	}

	/**
	 * Método de ejemplo.
	 * @return [type] [description]
	 */
	public function profile() {
		// $user = Auth::user();
		// $cliente = Cliente::WHERE('users_id',$user->id)->first();
		// $cliente = $user->id;
		// $nuevo = $cliente->usuario;
		// $serv = $cliente->servicioPrestado->count();
		// $rol = Rol::find(2);
		// $admins = $rol->clientes->count();
		// $pago_serv = PagoServicio::find(1);
		// $serv = $pago_serv->servicioPrestado;
		// 
		$serv = ServicioPrestado::find(1);
		$calificacion = $serv->calificacionServicio;
		return response()->json(compact('serv'), 200);
	}
}
