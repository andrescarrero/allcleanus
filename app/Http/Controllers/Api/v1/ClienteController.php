<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\ClienteController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClienteController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Cliente  $cliente
	 * @return \Illuminate\Http\Response
	 */
	public function show(Cliente $cliente) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Cliente  $cliente
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Cliente $cliente) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Cliente  $cliente
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Cliente $cliente) {
		//
	}
}
