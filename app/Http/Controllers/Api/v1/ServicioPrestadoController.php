<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\ServicioPrestado;
use Illuminate\Http\Request;

class ServicioPrestadoController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\ServicioPrestado  $servicioPrestado
	 * @return \Illuminate\Http\Response
	 */
	public function show(ServicioPrestado $servicioPrestado) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\ServicioPrestado  $servicioPrestado
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, ServicioPrestado $servicioPrestado) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\ServicioPrestado  $servicioPrestado
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(ServicioPrestado $servicioPrestado) {
		//
	}
}
