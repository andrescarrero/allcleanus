<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\Servicio;
use Illuminate\Http\Request;

class ServicioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Servicio  $servicio
	 * @return \Illuminate\Http\Response
	 */
	public function show(Servicio $servicio) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Servicio  $servicio
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Servicio $servicio) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Servicio  $servicio
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Servicio $servicio) {
		//
	}
}
