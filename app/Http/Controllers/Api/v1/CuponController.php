<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\Cupon;
use App\Models\Api\v1\PromocionServicio;
use App\Models\Api\v1\Servicio;
use App\Models\Api\v1\TipoCupon;
use App\Models\Api\v1\TipoServicio;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class CuponController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Cupon  $cupon
	 * @return \Illuminate\Http\Response
	 */
	public function show(Cupon $cupon) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Cupon  $cupon
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Cupon $cupon) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Cupon  $cupon
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Cupon $cupon) {
		//
	}

	/**
	 * Método para consultar si existe cupon aplicable para dicho servicio, en la fecha indicada
	 * @param  Request $request [date][cupon_code][service]
	 * @return [type]           [description]
	 */
	public function consultarCupon(Request $request) {
		try {
			//Se crea variable para retornar, si es true, devolverá el cupón encontrado, si es False, un msj de alerta
			$retornar = true;

			$alertaRetorno = "";

			//Fecha en la que solicitó el servicio
			if (strpos($request['date'], ':') !== false) {
				$fechaServicio = Carbon::createFromFormat('m-d-Y H:i:s', $request['date']);

			} else {
				$fechaServicio = Carbon::createFromFormat('m-d-Y', $request['date']);

			}

			//Buscamos que exista el cupón con una fecha superior o igual a la fecha de comienzo del cupon en cuestión
			$cupon = Cupon::where('clave', $request['cupon_code'])
				->whereDate('fecha_desde', "<=", $fechaServicio->toDateString())
				->where('cantidad_cupones', '>=', 1)
				->first();

			if ($cupon != null) {

				//En caso de que fecha_hasta sea diferente de null, se compara con la fecha del servicio
				if ($cupon['fecha_hasta'] != null) {
					$fecha_hasta = Carbon::createFromFormat('Y-m-d', $cupon['fecha_hasta']);
					//La fecha hasta desde ser menor o igual a la fecha de contrato del servicio
					if ($fechaServicio->lessThanOrEqualTo($fecha_hasta) == false) {
						$retornar      = false;
						$alertaRetorno = "The offer of this coupon has ended";
					}
				}

				//Si se cumplen todas las anteriores, y el cupon es diferente de Null, se debe saber si aplica para cualquier servicio, para ese tipo de servicio, o para ese servicio en particular.
				$tipoCupon = TipoCupon::where('id', $cupon['tipos_cupones_id'])->first();

				//Si no es un cupon "G" general, se debe verificar si es para tipo de servicio o para servicio en particular
				if ($tipoCupon['tipo_cupon'] != "G") {

					//Se reconoce el tipo de la promosión
					$promocion = PromocionServicio::where('cupones_id', $cupon['id'])->first();

					//Se busca el tipo de servicio del servicio donde se está usando el cupón
					$tipoServicio = TipoServicio::find(Servicio::find($request['service'])['tipos_servicios_id']);

					//Se determina si es para ese tipo de servicio
					if ($tipoCupon['tipo_cupon'] == "S" && $promocion['tipos_servicios_id'] != $tipoServicio['id']) {
						//El tipo de cupon es para otro tipo de servicio, no para el actual
						$retornar      = false;
						$alertaRetorno = "The coupon is only applicable to another type of service";
					}

					//Se determina si es para ese servicio en especifico
					if ($tipoCupon['tipo_cupon'] == "I" && $promocion['servicios_id'] != $request['service']) {
						//El tipo de cupon es para otro servicio en especifico, no para el actual
						$retornar      = false;
						$alertaRetorno = "The coupon is only applicable to another service";
					}

				}
			} else {
				$retornar      = false;
				$alertaRetorno = "The coupon code is not valid";
			}

			//Opción de retorno
			if ($retornar) {
				//se retorna el cupon encontrado
				return response()->json(["Cupon" => $cupon], 200);
			} else {
				//se retorna la alerta
				return response()->json(["Alerta" => $alertaRetorno], 200);
			}

		} catch (\Exception $e) {
			Log::critical("Ha ocurrido un problema al tratar de consultar el cupon {$e->getCode()} , {$e->getLine()} , {$e->getMessage()}");
			return response("Ha ocurrido un problema al tratar de consultar el  el cupon", 500);
		}
	}
}
