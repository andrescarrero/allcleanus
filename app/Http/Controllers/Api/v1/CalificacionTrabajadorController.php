<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\CalificacionTrabajador;
use Illuminate\Http\Request;

class CalificacionTrabajadorController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\CalificacionTrabajador  $calificacionTrabajador
	 * @return \Illuminate\Http\Response
	 */
	public function show(CalificacionTrabajador $calificacionTrabajador) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\CalificacionTrabajador  $calificacionTrabajador
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CalificacionTrabajador $calificacionTrabajador) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\CalificacionTrabajador  $calificacionTrabajador
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CalificacionTrabajador $calificacionTrabajador) {
		//
	}
}
