<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Api\v1\TipoDescuento;
use Illuminate\Http\Request;

class TipoDescuentoController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\TipoDescuento  $tipoDescuento
	 * @return \Illuminate\Http\Response
	 */
	public function show(TipoDescuento $tipoDescuento) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\TipoDescuento  $tipoDescuento
	 * @return \Illuminate\Http\Response
	 */
	public function edit(TipoDescuento $tipoDescuento) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\TipoDescuento  $tipoDescuento
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, TipoDescuento $tipoDescuento) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\TipoDescuento  $tipoDescuento
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TipoDescuento $tipoDescuento) {
		//
	}
}
