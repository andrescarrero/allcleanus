<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Clientes</title>
</head>

<body>
    <form action="{{ url('/oauth/clients') }}" method="POST">
        <p>
            <input type="text" name="name">
        </p>
        <p>
            <input type="text" name="redirect">
        </p>
        <p>
            <input type="submit" name="send" value="Enviar">
        </p>
        {{ csrf_field() }}
    </form>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Redirect</th>
                <th>Secret</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clients as $client)
            <tr>
                <td>{{ $client->id }}</td>
                <td>{{ $client->name }}</td>
                <td>{{ $client->redirect }}</td>
                <td>{{ $client->secret }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>