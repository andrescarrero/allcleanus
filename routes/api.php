<?php

use App\Models\Api\v1\Servicio;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware(['auth:api'])->prefix('v1')->group(function () {

	Route::get('profile', 'Api\v1\AuthController@profile');

	Route::apiResource('roles', 'Api\v1\RolController');

}); //->middleware('auth:api');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Client Credentials
Route::get('/list/services', function () {
	return Servicio::all();
})->middleware('client');

Route::post('/list/services', function (Request $request) {
	Servicio::create([
		'servicio'           => $request->input('servicio'),
		'porc_trabajador'    => $request->input('porc_trabajador'),
		'porc_tercero'       => $request->input('porc_tercero'),
		'tipos_servicios_id' => $request->input('tipos_servicios_id'),
	]);

	return ['status' => 200];
})->middleware('client');

Route::get('services', function () {
	return Servicio::find(1);
});

Route::prefix('v1')->group(function () {

	Route::post('registro', 'Api\v1\AuthController@register');

	Route::post('login', 'Api\v1\AuthController@login');

	Route::get('/list/services', function () {
		$services = Servicio::all();
		return response()->json(compact('services'), 200);
	});

});

Route::get('consultarHomeCleaning', 'Api\v1\PrecioServicioController@consultarPrecioHomeCleaning');

Route::get('consultarOfficeCleaning', 'Api\v1\PrecioServicioController@consultarPrecioOfficeCleaning');

Route::get('consultarGarageCleaning', 'Api\v1\PrecioServicioController@consultarPrecioGarageCleaning');

Route::get('consultarPaintingService', 'Api\v1\PrecioServicioController@consultarPrecioPaintingService');

Route::get('consultarPaintingFrontDoor', 'Api\v1\PrecioServicioController@consultarPrecioPaintingFrontDoor');

Route::post('consultarCupon', 'Api\v1\CuponController@consultarCupon');

Route::post('consultarDescuento', 'Api\v1\DescuentoController@consultarDescuento');
