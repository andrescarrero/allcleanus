<?php

use App\Models\Api\v1\EstadoServicio;
use Illuminate\Database\Seeder;

class EstadosServiciosTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'nombre'      => 'P',
				'descripcion' => 'Status of the PENDING service to be assigned corresponds to a service requested by a client that does not yet have an assigned worker',
			],
			[
				'nombre'      => 'A',
				'descripcion' => 'ASSIGNED service status, corresponds to a service requested by a client that already an administrator was responsible for finding the necessary resource',
			],
			[
				'nombre'      => 'C',
				'descripcion' => 'COMPLETED service status, corresponds to a service that has already been executed',
			],
		);

		EstadoServicio::insert($data);
	}
}
