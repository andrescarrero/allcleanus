<?php

use App\Models\Api\v1\TipoDescuento;
use Illuminate\Database\Seeder;

class TiposDescuentosTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'tipo_descuento' => 'G',
				'descripcion'    => 'General, discount applicable to any service',
			],
			[
				'tipo_descuento' => 'S',
				'descripcion'    => 'Type Service, discount applicable by type of service',
			],
			[
				'tipo_descuento' => 'I',
				'descripcion'    => 'Individual, discount applicable to a particular service',
			],
		);
		TipoDescuento::insert($data);
	}
}
