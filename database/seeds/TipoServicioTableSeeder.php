<?php

use App\Models\Api\v1\TipoServicio;
use Illuminate\Database\Seeder;

class TipoServicioTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'tipo_servicio' => 'Cleaning Services', //1
			],
			[
				'tipo_servicio' => 'Painting Services', //2
			],
		);
		TipoServicio::insert($data);
	}
}
