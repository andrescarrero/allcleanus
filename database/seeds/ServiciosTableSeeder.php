<?php

use App\Models\Api\v1\Servicio;
use Illuminate\Database\Seeder;

class ServiciosTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'servicio'           => 'Home Cleaning', //1
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 1,
			],
			[
				'servicio'           => 'Deep Cleaning', //2
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 1,
			],
			[
				'servicio'           => 'Cleaning Garage', //3
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 1,
			],
			[
				'servicio'           => 'Office Cleaning', //4
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 1,
			],
			[
				'servicio'           => 'Windows Cleaning', //5
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 1,
			],
			[
				'servicio'           => 'Painting Service', //6
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 2,
			],
			[
				'servicio'           => 'Painting Garage', //7
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 2,
			],
			[
				'servicio'           => 'Painting Front Door', //8
				'porc_trabajador'    => '50',
				'porc_tercero'       => '60',
				'tipos_servicios_id' => 2,
			],
		);
		Servicio::insert($data);
	}
}
