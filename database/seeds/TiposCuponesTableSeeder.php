<?php

use App\Models\Api\v1\TipoCupon;
use Illuminate\Database\Seeder;

class TiposCuponesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'tipo_cupon'  => 'G',
				'descripcion' => 'General, cupon applicable to any service',
			],
			[
				'tipo_cupon'  => 'S',
				'descripcion' => 'Type Service, cupon applicable by type of service',
			],
			[
				'tipo_cupon'  => 'I',
				'descripcion' => 'Individual, cupon applicable to a particular service',
			],
		);
		TipoCupon::insert($data);
	}
}
