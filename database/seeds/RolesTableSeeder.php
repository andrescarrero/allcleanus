<?php

use App\Models\Api\v1\Rol;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'nombre' => 'Administrator',
			],
			[
				'nombre' => 'Client',
			],
		);
		Rol::insert($data);
	}
}
