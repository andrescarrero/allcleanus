<?php

use App\Models\Api\v1\Atributo;
use Illuminate\Database\Seeder;

class AtributosTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'descripcion' => 'Beds', //1
			],
			[
				'descripcion' => 'Baths', //2
			],
			[
				'descripcion' => 'Deep cleaning bathroom? (standard size)', //3
			],
			[
				'descripcion' => 'Deep cleaning fridge? (standard size)', //4
			],
			[
				'descripcion' => 'Deep cleaning garage? (standard size)', //5
			],
			[
				'descripcion' => 'Deep cleaning kitchen? (standard size)', //6
			],
			[
				'descripcion' => 'deep cleaning oven? (standard size)', //7
			],
			[
				'descripcion' => 'Painting Included', //8
			],
			[
				'descripcion' => 'Painting Garage', //9
			],
			[
				'descripcion' => 'Painting Front Door', //10
			],
			[
				'descripcion' => 'What is the size of the office to be cleaned?', //11
			],
			[
				'descripcion' => 'Size', //12
			],
			// [
			// 	'descripcion' => 'What do you need power washed?', //9
			// ],
			// [
			// 	'descripcion' => 'What size is the area you need washed?', //10
			// ],
			// [
			// 	'descripcion' => 'How many windows do you need cleaned?', //11
			// ],
			// [
			// 	'descripcion' => 'What needs to be cleaned?', //12
			// ],
			// [
			// 	'descripcion' => 'How many stories of windows do you need cleaned?', //13
			// ],
		);

		Atributo::insert($data);
	}
}
