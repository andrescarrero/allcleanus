<?php

use App\Models\Api\v1\CalificacionServicio;
use App\Models\Api\v1\CalificacionTrabajador;
use App\Models\Api\v1\Cliente;
use App\Models\Api\v1\Cupon;
use App\Models\Api\v1\Descuento;
use App\Models\Api\v1\Empresa;
use App\Models\Api\v1\PagoServicio;
use App\Models\Api\v1\PagoTrabajador;
use App\Models\Api\v1\PromocionServicio;
use App\Models\Api\v1\ServicioPrestado;
use App\Models\Api\v1\ServicioTrabajador;
use App\Models\Api\v1\Trabajador;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {

		// $cant_users                     = 10000;
		// $cant_calificaciones_trabajador = 1000;
		// $cant_trabajador                = 5000;
		// $cant_empresas                  = 20;
		// $cant_pagos_servicios           = 1000;
		// $cant_calificaciones_servicios  = 200;
		// $cant_descuento                 = 50;
		// $cant_cupones                   = 50;
		// $cant_clientes                  = 2000;
		// $cant_pagos_trabajadores        = 50;
		// $cant_cupones_servicio          = 50;
		// $cant_servicios_trabajador      = 300;
		// $cant_servicios_prestados       = 10000;

		$cant_users                     = 1;
		$cant_calificaciones_trabajador = 1;
		$cant_trabajador                = 1;
		$cant_empresas                  = 1;
		$cant_pagos_servicios           = 1;
		$cant_calificaciones_servicios  = 1;
		$cant_descuento                 = 1;
		$cant_cupones                   = 1;
		$cant_clientes                  = 1;
		$cant_pagos_trabajadores        = 1;
		$cant_cupones_servicio          = 1;
		$cant_servicios_trabajador      = 1;
		$cant_servicios_prestados       = 1;

		//Creando los usuarios
		factory(User::class, $cant_users)->create(); //10000

		//Creando los estados de servicios
		$this->call(EstadosServiciosTableSeeder::class);

		//Creando los tipos de servicios
		$this->call(TipoServicioTableSeeder::class);

		//Creando "N" cantidad de calificaciones a trabajadores
		factory(CalificacionTrabajador::class, $cant_calificaciones_trabajador)->create(); //1000

		//Creando los roles de usuarios
		$this->call(RolesTableSeeder::class);

		//Creando los tipos de descuentos
		$this->call(TiposDescuentosTableSeeder::class);

		//Creando los tipo de cupones
		$this->call(TiposCuponesTableSeeder::class);

		//Creando "N" cantidad de pagos de servicios
		factory(PagoServicio::class, $cant_pagos_servicios)->create(); //1000

		//Creando las empresas
		factory(Empresa::class, $cant_empresas)->create(); //20

		//Creando las posibles especificaciones de los servicios
		$this->call(EspecificacionesTableSeeder::class);

		//Creando "N" Calificaciones a servicios
		factory(CalificacionServicio::class, $cant_calificaciones_servicios)->create(); //200

		//Creando las posibles especificaciones de los servicios
		$this->call(AtributosTableSeeder::class);

		//Creando los servicios prestados por la empresa
		$this->call(ServiciosTableSeeder::class);

		//Creando "N" cantidad de descuentos
		factory(Descuento::class, $cant_descuento)->create(); //50

		//Creando "N" Cupones
		factory(Cupon::class, $cant_cupones)->create(); //50

		//Creando "N" cantidad de trabajadores
		factory(Trabajador::class, $cant_trabajador)->create(); //5000

		//Creando "N" Clientes
		factory(Cliente::class, $cant_clientes)->create(); //2000

		//Creando los precios de los diferentes servicios
		$this->call(PrecioServicioTableSeeder::class);

		//Creando "N" Cupones afiliados a ciertos servicios
		factory(PromocionServicio::class, $cant_cupones_servicio)->create(); //50

		//Creando "N" Servicios a trabajadores
		factory(ServicioTrabajador::class, $cant_servicios_trabajador)->create(); //300

		//Creando "N" Pagos a trabajadores
		factory(PagoTrabajador::class, $cant_pagos_trabajadores)->create(); //50

		//Creando "N" Servicios a prestados
		factory(ServicioPrestado::class, $cant_servicios_prestados)->create(); //10000
	}
}
