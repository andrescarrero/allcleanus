<?php

use App\Models\Api\v1\PrecioServicio;
use Illuminate\Database\Seeder;

class PrecioServicioTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[ //1
				'servicios_id'         => 1, //Home Cleaning - Padre - 0 Beds 0 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 0,
				'precios_servicios_id' => null,
			],
			[ //2
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 1, //Padre
			],
			[ //3
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 1, //Padre
			],
			[ //4
				'servicios_id'         => 1, //Home Cleaning - Padre - 0 Beds 1 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 18,
				'precios_servicios_id' => null,
			],
			[ //5
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 4, //Padre
			],
			[ //6
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 4, //Padre
			],
			[ //7
				'servicios_id'         => 1, //Home Cleaning - Padre - 0 Beds 2 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 30,
				'precios_servicios_id' => null,
			],
			[ //8
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 7, //Padre
			],
			[ //9
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 7, //Padre
			],
			[ //10
				'servicios_id'         => 1, //Home Cleaning - Padre - 0 Beds 3 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 45,
				'precios_servicios_id' => null,
			],
			[ //11
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 10, //Padre
			],
			[ //12
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 10, //Padre
			],
			[ //13
				'servicios_id'         => 1, //Home Cleaning - Padre - 0 Beds 4 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 60,
				'precios_servicios_id' => null,
			],
			[ //14
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 13, //Padre
			],
			[ //15
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 13, //Padre
			],
			[ //16
				'servicios_id'         => 1, //Home Cleaning - Padre - 1 Beds 0 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 18,
				'precios_servicios_id' => null,
			],
			[ //17
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 16, //Padre
			],
			[ //18
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 16, //Padre
			],
			[ //19
				'servicios_id'         => 1, //Home Cleaning - Padre - 2 Beds 0 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 30,
				'precios_servicios_id' => null,
			],
			[ //20
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 19, //Padre
			],
			[ //21
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 19, //Padre
			],
			[ //22
				'servicios_id'         => 1, //Home Cleaning - Padre - 3 Beds 0 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 45,
				'precios_servicios_id' => null,
			],
			[ //23
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 22, //Padre
			],
			[ //24
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 22, //Padre
			],
			[ //25
				'servicios_id'         => 1, //Home Cleaning - Padre - 4 Beds 0 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 60,
				'precios_servicios_id' => null,
			],
			[ //26
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 5, //4
				'precio'               => null,
				'precios_servicios_id' => 25, //Padre
			],
			[ //27
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 25, //Padre
			],
			[ //28
				'servicios_id'         => 1, //Home Cleaning - Padre - 5 Beds 0 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 75,
				'precios_servicios_id' => null,
			],
			[ //29
				'servicios_id'         => 1, //Home Cleaning - Hijo - 5 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 6, //5
				'precio'               => null,
				'precios_servicios_id' => 28, //Padre
			],
			[ //30
				'servicios_id'         => 1, //Home Cleaning - Hijo - 0 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 1, // 0
				'precio'               => null,
				'precios_servicios_id' => 28, //Padre
			],
			[ //31
				'servicios_id'         => 1, //Home Cleaning - Padre - 1 Beds 1 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 80,
				'precios_servicios_id' => null,
			],
			[ //32
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 31, //Padre
			],
			[ //33
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 31, //Padre
			],
			[ //34
				'servicios_id'         => 1, //Home Cleaning - Padre - 1 Beds 2 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 93,
				'precios_servicios_id' => null,
			],
			[ //35
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 34, //Padre
			],
			[ //36
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 34, //Padre
			],
			[ //37
				'servicios_id'         => 1, //Home Cleaning - Padre - 1 Beds 3 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 108,
				'precios_servicios_id' => null,
			],
			[ //38
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 2, //1
				'precio'               => null,
				'precios_servicios_id' => 37, //Padre
			],
			[ //39
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 37, //Padre
			],
			[ //40
				'servicios_id'         => 1, //Home Cleaning - Padre - 1 Beds 4 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 120,
				'precios_servicios_id' => null,
			],
			[ //41
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 40, //Padre
			],
			[ //42
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 40, //Padre
			],
			[ //43
				'servicios_id'         => 1, //Home Cleaning - Padre - 2 Beds 1 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 93,
				'precios_servicios_id' => null,
			],
			[ //44
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 43, //Padre
			],
			[ //45
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 43, //Padre
			],
			[ //46
				'servicios_id'         => 1, //Home Cleaning - Padre - 2 Beds 2 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 108,
				'precios_servicios_id' => null,
			],
			[ //47
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 46, //Padre
			],
			[ //48
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 46, //Padre
			],
			[ //49
				'servicios_id'         => 1, //Home Cleaning - Padre - 2 Beds 3 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 120,
				'precios_servicios_id' => null,
			],
			[ //50
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 49, //Padre
			],
			[ //51
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 49, //Padre
			],
			[ //52
				'servicios_id'         => 1, //Home Cleaning - Padre - 2 Beds 4 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 135,
				'precios_servicios_id' => null,
			],
			[ //53
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 52, //Padre
			],
			[ //54
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 52, //Padre
			],
			[ //55
				'servicios_id'         => 1, //Home Cleaning - Padre - 3 Beds 1 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 108,
				'precios_servicios_id' => null,
			],
			[ //56
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 55, //Padre
			],
			[ //57
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 55, //Padre
			],
			[ //58
				'servicios_id'         => 1, //Home Cleaning - Padre - 3 Beds 2 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 120,
				'precios_servicios_id' => null,
			],
			[ //59
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 58, //Padre
			],
			[ //60
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 58, //Padre
			],
			[ //61
				'servicios_id'         => 1, //Home Cleaning - Padre - 3 Beds 3 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 135,
				'precios_servicios_id' => null,
			],
			[ //62
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 61, //Padre
			],
			[ //63
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 61, //Padre
			],
			[ //64
				'servicios_id'         => 1, //Home Cleaning - Padre - 3 Beds 4 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 150,
				'precios_servicios_id' => null,
			],
			[ //65
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 64, //Padre
			],
			[ //66
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 64, //Padre
			],
			[ //67
				'servicios_id'         => 1, //Home Cleaning - Padre - 4 Beds 1 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 120,
				'precios_servicios_id' => null,
			],
			[ //68
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 67, //Padre
			],
			[ //69
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 67, //Padre
			],
			[ //70
				'servicios_id'         => 1, //Home Cleaning - Padre - 4 Beds 2 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 135,
				'precios_servicios_id' => null,
			],
			[ //71
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 70, //Padre
			],
			[ //72
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 70, //Padre
			],
			[ //73
				'servicios_id'         => 1, //Home Cleaning - Padre - 4 Beds 3 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 150,
				'precios_servicios_id' => null,
			],
			[ //74
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 73, //Padre
			],
			[ //75
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 73, //Padre
			],
			[ //76
				'servicios_id'         => 1, //Home Cleaning - Padre - 4 Beds 4 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 165,
				'precios_servicios_id' => null,
			],
			[ //77
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 76, //Padre
			],
			[ //78
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 76, //Padre
			],
			[ //79
				'servicios_id'         => 1, //Home Cleaning - Padre - 5 Beds 1 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 135,
				'precios_servicios_id' => null,
			],
			[ //80
				'servicios_id'         => 1, //Home Cleaning - Hijo - 5 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 6, // 5
				'precio'               => null,
				'precios_servicios_id' => 79, //Padre
			],
			[ //81
				'servicios_id'         => 1, //Home Cleaning - Hijo - 1 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 2, // 1
				'precio'               => null,
				'precios_servicios_id' => 79, //Padre
			],
			[ //82
				'servicios_id'         => 1, //Home Cleaning - Padre - 5 Beds 2 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 150,
				'precios_servicios_id' => null,
			],
			[ //83
				'servicios_id'         => 1, //Home Cleaning - Hijo - 5 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 6, // 5
				'precio'               => null,
				'precios_servicios_id' => 82, //Padre
			],
			[ //84
				'servicios_id'         => 1, //Home Cleaning - Hijo - 2 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 3, // 2
				'precio'               => null,
				'precios_servicios_id' => 82, //Padre
			],
			[ //85
				'servicios_id'         => 1, //Home Cleaning - Padre - 5 Beds 3 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 165,
				'precios_servicios_id' => null,
			],
			[ //86
				'servicios_id'         => 1, //Home Cleaning - Hijo - 5 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 6, // 5
				'precio'               => null,
				'precios_servicios_id' => 85, //Padre
			],
			[ //87
				'servicios_id'         => 1, //Home Cleaning - Hijo - 3 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 4, // 3
				'precio'               => null,
				'precios_servicios_id' => 85, //Padre
			],
			[ //88
				'servicios_id'         => 1, //Home Cleaning - Padre - 5 Beds 4 Baths
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 180,
				'precios_servicios_id' => null,
			],
			[ //89
				'servicios_id'         => 1, //Home Cleaning - Hijo - 5 Beds
				'atributos_id'         => 1, //Beds
				'especificaciones_id'  => 6, // 5
				'precio'               => null,
				'precios_servicios_id' => 88, //Padre
			],
			[ //90
				'servicios_id'         => 1, //Home Cleaning - Hijo - 4 Baths
				'atributos_id'         => 2, //Baths
				'especificaciones_id'  => 5, // 4
				'precio'               => null,
				'precios_servicios_id' => 88, //Padre

				//FIN HOME CLEANING
			],
			[ //91
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(no), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 45,
				'precios_servicios_id' => null,
			],
			[ //92
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 91,
			],
			[ //93
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 91,
			],
			[ //94
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 91,
			],
			[ //95
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 91,
			],
			[ //96
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 91,
			],
			[ //97
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(no), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 90,
				'precios_servicios_id' => null,
			],
			[ //98
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 97,
			],
			[ //99
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 97,
			],
			[ //100
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 97,
			],
			[ //101
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 97,
			],
			[ //102
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 97,
			],
			[ //103
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(no), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 125,
				'precios_servicios_id' => null,
			],
			[ //104
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 103,
			],
			[ //105
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 103,
			],
			[ //106
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 103,
			],
			[ //107
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 103,
			],
			[ //108
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 103,
			],
			[ //109
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(no), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 170,
				'precios_servicios_id' => null,
			],
			[ //110
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 109,
			],
			[ //111
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 109,
			],
			[ //112
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 109,
			],
			[ //113
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 109,
			],
			[ //114
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 109,
			],
			[ //115
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(yes), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 175,
				'precios_servicios_id' => null,
			],
			[ //116
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 115,
			],
			[ //117
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 115,
			],
			[ //118
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 115,
			],
			[ //119
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 115,
			],
			[ //120
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 115],
			[ //121
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(yes), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 220,
				'precios_servicios_id' => null,
			],
			[ //122
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 121,
			],
			[ //123
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 121,
			],
			[ //124
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 121,
			],
			[ //125
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 121,
			],
			[ //126
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 121],
			[ //127
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(yes), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 255,
				'precios_servicios_id' => null,
			],
			[ //128
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 127,
			],
			[ //129
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 127,
			],
			[ //130
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 127,
			],
			[ //131
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 127,
			],
			[ //132
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 127,
			],
			[ //133
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(no), garage(yes), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 300,
				'precios_servicios_id' => null,
			],
			[ //134
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 133,
			],
			[ //135
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 133,
			],
			[ //136
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 133,
			],
			[ //137
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 133,
			],
			[ //138
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 133,
			],
			[ //139
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(no), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 90,
				'precios_servicios_id' => null,
			],
			[ //140
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 139,
			],
			[ //141
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 139,
			],
			[ //142
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 139,
			],
			[ //143
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 139,
			],
			[ //144
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 139,
			],
			[ //145
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(no), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 135,
				'precios_servicios_id' => null,
			],
			[ //146
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 145,
			],
			[ //147
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 145,
			],
			[ //148
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 145,
			],
			[ //149
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 145,
			],
			[ //150
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 145,
			],
			[ //151
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(no), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 170,
				'precios_servicios_id' => null,
			],
			[ //152
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 151,
			],
			[ //153
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 151,
			],
			[ //154
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 151,
			],
			[ //155
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 151,
			],
			[ //156
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 151,
			],
			[ //157
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(no), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 215,
				'precios_servicios_id' => null,
			],
			[ //158
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 157,
			],
			[ //159
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 157,
			],
			[ //160
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 157,
			],
			[ //161
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 157,
			],
			[ //162
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 157,
			],
			[ //163
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(yes), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 220,
				'precios_servicios_id' => null,
			],
			[ //164
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 163,
			],
			[ //165
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 163,
			],
			[ //166
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 163,
			],
			[ //167
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 163,
			],
			[ //168
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 163,
			],
			[ //169
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(yes), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 265,
				'precios_servicios_id' => null,
			],
			[ //170
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 169,
			],
			[ //171
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 169,
			],
			[ //172
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 169,
			],
			[ //173
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 169,
			],
			[ //174
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 169,
			],
			[ //175
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(yes), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 300,
				'precios_servicios_id' => null,
			],
			[ //176
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 175,
			],
			[ //177
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 175,
			],
			[ //178
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 175,
			],
			[ //179
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 175,
			],
			[ //180
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 175,
			],
			[ //181
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes), fridge(yes), garage(yes), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 345,
				'precios_servicios_id' => null,
			],
			[ //182
				'servicios_id'         => 2, //Deep Cleaning bathroom(yes)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 181,
			],
			[ //183
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 181,
			],
			[ //184
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 181,
			],
			[ //185
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 181,
			],
			[ //186
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 181,
			],
			[ //187
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(no), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 0,
				'precios_servicios_id' => null,
			],
			[ //188
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 187,
			],
			[ //189
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 187,
			],
			[ //190
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 187,
			],
			[ //191
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 187,
			],
			[ //192
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 187,
			],
			[ //193
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(no), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 40,
				'precios_servicios_id' => null,
			],
			[ //194
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 193,
			],
			[ //195
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 193,
			],
			[ //196
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 193,
			],
			[ //197
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 193,
			],
			[ //198
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 193,
			],
			[ //199
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(no), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 80,
				'precios_servicios_id' => null,
			],
			[ //200
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 199,
			],
			[ //201
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 199,
			],
			[ //202
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 199,
			],
			[ //203
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 199,
			],
			[ //204
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 199,
			],
			[ //205
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(no), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 125,
				'precios_servicios_id' => null,
			],
			[ //206
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 205,
			],
			[ //207
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 205,
			],
			[ //208
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 205,
			],
			[ //209
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 205,
			],
			[ //210
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 205,
			],
			[ //211
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(yes), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 130,
				'precios_servicios_id' => null,
			],
			[ //212
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 211,
			],
			[ //213
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 211,
			],
			[ //214
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 211,
			],
			[ //215
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 211,
			],
			[ //216
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 211,
			],
			[ //217
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(yes), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 175,
				'precios_servicios_id' => null,
			],
			[ //218
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 217,
			],
			[ //219
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 217,
			],
			[ //220
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 217,
			],
			[ //221
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 217,
			],
			[ //222
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 217,
			],
			[ //223
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(yes), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 210,
				'precios_servicios_id' => null,
			],
			[ //224
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 223,
			],
			[ //225
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 223,
			],
			[ //226
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 223,
			],
			[ //227
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 223,
			],
			[ //228
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 223,
			],
			[ //229
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(no), garage(yes), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 255,
				'precios_servicios_id' => null,
			],
			[ //230
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 229,
			],
			[ //231
				'servicios_id'         => 2, //Deep Cleaning fridge(no)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 229,
			],
			[ //232
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 229,
			],
			[ //233
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 229,
			],
			[ //234
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 229,
			],
			[ //235
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(no), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 45,
				'precios_servicios_id' => null,
			],
			[ //236
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 235,
			],
			[ //237
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 235,
			],
			[ //238
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 235,
			],
			[ //239
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 235,
			],
			[ //240
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 235],
			[ //241
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(no ), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 90,
				'precios_servicios_id' => null,
			],
			[ //242
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 241,
			],
			[ //243
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 241,
			],
			[ //244
				'servicios_id'         => 2, //Deep Cleaning garage(no )
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 241,
			],
			[ //245
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 241,
			],
			[ //246
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 241,
			],
			[ //247
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(no), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 125,
				'precios_servicios_id' => null,
			],
			[ //248
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 247,
			],
			[ //249
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 247,
			],
			[ //250
				'servicios_id'         => 2, //Deep Cleaning garage(no)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 247,
			],
			[ //251
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 247,
			],
			[ //252
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 247],
			[ //253
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(no ), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 165,
				'precios_servicios_id' => null,
			],
			[ //254
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 253,
			],
			[ //255
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 253,
			],
			[ //256
				'servicios_id'         => 2, //Deep Cleaning garage(no )
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 253,
			],
			[ //257
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 253,
			],
			[ //258
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 253],
			[ //259
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(yes), kitchen(no), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 175,
				'precios_servicios_id' => null,
			],
			[ //260
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 259,
			],
			[ //261
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 259,
			],
			[ //262
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 259,
			],
			[ //263
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 259,
			],
			[ //264
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 259,
			],
			[ //265
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(yes), kitchen(no), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 220,
				'precios_servicios_id' => null,
			],
			[ //266
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 265,
			],
			[ //267
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 265,
			],
			[ //268
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 265,
			],
			[ //269
				'servicios_id'         => 2, //Deep Cleaning kitchen(no)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 265,
			],
			[ //270
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 265],
			[ //271
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(yes), kitchen(yes), oven(no)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 255,
				'precios_servicios_id' => null,
			],
			[ //272
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 271,
			],
			[ //273
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 271,
			],
			[ //274
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 271,
			],
			[ //275
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 271,
			],
			[ //276
				'servicios_id'         => 2, //Deep Cleaning oven(no)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 271],
			[ //277
				'servicios_id'         => 2, //Deep Cleaning bathroom(no), fridge(yes), garage(yes), kitchen(yes), oven(yes)
				'atributos_id'         => null,
				'especificaciones_id'  => null,
				'precio'               => 300,
				'precios_servicios_id' => null,
			],
			[ //278
				'servicios_id'         => 2, //Deep Cleaning bathroom(no)
				'atributos_id'         => 3, //bathroom
				'especificaciones_id'  => 7, //no
				'precio'               => null,
				'precios_servicios_id' => 277,
			],
			[ //279
				'servicios_id'         => 2, //Deep Cleaning fridge(yes)
				'atributos_id'         => 4, //fridge
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 277,
			],
			[ //280
				'servicios_id'         => 2, //Deep Cleaning garage(yes)
				'atributos_id'         => 5, //garaje
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 277,
			],
			[ //281
				'servicios_id'         => 2, //Deep Cleaning kitchen(yes)
				'atributos_id'         => 6, //kitchen
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 277,
			],
			[ //282
				'servicios_id'         => 2, //Deep Cleaning oven(yes)
				'atributos_id'         => 7, //oven
				'especificaciones_id'  => 8, //yes
				'precio'               => null,
				'precios_servicios_id' => 277,
			],
			[ //283
				'servicios_id'         => 6, //Painting Service
				'atributos_id'         => 8, //Painting Included
				'especificaciones_id'  => 9, //Base Paint
				'precio'               => 0.37,
				'precios_servicios_id' => null,
			],
			[ //284
				'servicios_id'         => 6, //Painting Service
				'atributos_id'         => 8, //Painting Included
				'especificaciones_id'  => 10, //Ceiling - Wall - Doors - Base Board
				'precio'               => 1.67,
				'precios_servicios_id' => null,
			],
			[ //285
				'servicios_id'         => 6, //Painting Service
				'atributos_id'         => 8, //Painting Included
				'especificaciones_id'  => 11, //Only Wall
				'precio'               => 1.55,
				'precios_servicios_id' => null,
			],
			[ //286
				'servicios_id'         => 7, //Painting Garage
				'atributos_id'         => 9, //Painting Garage
				'especificaciones_id'  => 12, //Static
				'precio'               => 390,
				'precios_servicios_id' => null,
			],
			[ //287
				'servicios_id'         => 8, //Painting Front Door
				'atributos_id'         => 10, //Painting Front Door
				'especificaciones_id'  => 12, //Static
				'precio'               => 195,
				'precios_servicios_id' => null,
			],
			[ //288
				'servicios_id'         => 4, //Office Cleaning
				'atributos_id'         => 11, //What is the size of the office to be cleaned
				'especificaciones_id'  => 13, //0-900 sq ft
				'precio'               => 45,
				'precios_servicios_id' => null,
			],
			[ //289
				'servicios_id'         => 4, //Office Cleaning
				'atributos_id'         => 11, //What is the size of the office to be cleaned
				'especificaciones_id'  => 14, //901-1200 sq ft
				'precio'               => 145,
				'precios_servicios_id' => null,
			],
			[ //290
				'servicios_id'         => 4, //Office Cleaning
				'atributos_id'         => 11, //What is the size of the office to be cleaned
				'especificaciones_id'  => 15, //1201-1400 sq ft
				'precio'               => 65,
				'precios_servicios_id' => null,
			],
			[ //291
				'servicios_id'         => 4, //Office Cleaning
				'atributos_id'         => 11, //What is the size of the office to be cleaned
				'especificaciones_id'  => 16, //1401-1600 sq ft
				'precio'               => 85,
				'precios_servicios_id' => null,
			],
			[ //292
				'servicios_id'         => 4, //Office Cleaning
				'atributos_id'         => 11, //What is the size of the office to be cleaned
				'especificaciones_id'  => 17, //1601-1800 sq ft
				'precio'               => 105,
				'precios_servicios_id' => null,
			],
			[ //293
				'servicios_id'         => 4, //Office Cleaning
				'atributos_id'         => 11, //What is the size of the office to be cleaned
				'especificaciones_id'  => 18, //1801-200 sq ft
				'precio'               => 125,
				'precios_servicios_id' => null,
			],
			[ //294
				'servicios_id'         => 3, //Garage Cleaning
				'atributos_id'         => 12, //Size
				'especificaciones_id'  => 19, //Small
				'precio'               => 45,
				'precios_servicios_id' => null,
			],			
			[ //295
				'servicios_id'         => 3, //Garage Cleaning
				'atributos_id'         => 12, //Size
				'especificaciones_id'  => 20, //Medium
				'precio'               => 60,
				'precios_servicios_id' => null,
			],
			[ //296
				'servicios_id'         => 3, //Garage Cleaning
				'atributos_id'         => 12, //Size
				'especificaciones_id'  => 21, //Big
				'precio'               => 180,
				'precios_servicios_id' => null,
			],
		);
		PrecioServicio::insert($data);
	}
}
