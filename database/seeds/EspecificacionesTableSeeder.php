<?php

use App\Models\Api\v1\Especificacion;
use Illuminate\Database\Seeder;

class EspecificacionesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$data = array(
			[
				'descripcion' => '0', //1
			],
			[
				'descripcion' => '1', //2
			],
			[
				'descripcion' => '2', //3
			],
			[
				'descripcion' => '3', //4
			],
			[
				'descripcion' => '4', //5
			],
			[
				'descripcion' => '5', //6
			],
			[
				'descripcion' => 'No', //7
			],
			[
				'descripcion' => 'Yes', //8
			],
			[
				'descripcion' => 'Base Paint', //9
			],
			[
				'descripcion' => 'Ceiling - Wall - Doors - Base Board', //10
			],
			[
				'descripcion' => 'Only Wall', //11
			],
			[
				'descripcion' => 'Static', //12
			],
			[
				'descripcion' => '0-900 sq ft', //13
			],
			[
				'descripcion' => '901-1200 sq ft', //14
			],
			[
				'descripcion' => '1201-1400 sq ft', //15
			],
			[
				'descripcion' => '1401-1600 sq ft', //16
			],
			[
				'descripcion' => '1601-1800 sq ft', //17
			],
			[
				'descripcion' => '1801-2000 sq ft', //18
			],
			[
				'descripcion' => 'Small', //19
			],
			[
				'descripcion' => 'Medium', //20
			],
			[
				'descripcion' => 'Big', //21
			],
			// [
			// 	'descripcion' => 'Building Exterior', //16
			// ],
			// [
			// 	'descripcion' => 'Decking', //17
			// ],
			// [
			// 	'descripcion' => 'Driveway', //18
			// ],
			// [
			// 	'descripcion' => '2000-4000 sq ft', //19
			// ],
			// [
			// 	'descripcion' => 'Less Than 2000 sq ft', //20
			// ],
			// [
			// 	'descripcion' => '1-10', //21
			// ],
			// [
			// 	'descripcion' => '11-20', //22
			// ],
			// [
			// 	'descripcion' => '21-30', //23
			// ],
			// [
			// 	'descripcion' => '31-40', //24
			// ],
			// [
			// 	'descripcion' => '41-50', //25
			// ],
			// [
			// 	'descripcion' => 'Both Sides + Screens + Sills/Tracks/Frames', //26
			// ],
			// [
			// 	'descripcion' => 'Exterior + Interior Panes', //27
			// ],
			// [
			// 	'descripcion' => 'Exterior Panes Only', //28
			// ],
			// [
			// 	'descripcion' => '1 Story', //29
			// ],
			// [
			// 	'descripcion' => '2 Stories', //30
			// ],
			// [
			// 	'descripcion' => '5', //35
			// ],
		);

		Especificacion::insert($data);
	}
}
