<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\Empresa::class, function (Faker $faker) {
	return [
		'nombre'     => $faker->streetName . " Company",
		'telefono'   => $faker->phoneNumber,
		'telefono_2' => $faker->phoneNumber,
		'direccion'  => $faker->latitude . "," . $faker->longitude,
	];
});
