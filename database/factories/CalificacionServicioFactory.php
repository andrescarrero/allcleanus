<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\CalificacionServicio::class, function (Faker $faker) {
	return [
		'calificacion'    => $faker->numberBetween($min = 1, $max = 5),
		'otro_trabajador' => $faker->name,
		'comentario'      => $faker->sentence($nbWords = 10, $variableNbWords = true),
	];
});
