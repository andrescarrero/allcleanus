<?php

use App\Models\Api\v1\TipoCupon;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\Cupon::class, function (Faker $faker) {
	return [
		'clave'                => $faker->regexify('[0-9]{10}'),
		'cantidad_cupones'     => $faker->numberBetween($min = 0, $max = 100),
		'fecha_desde'          => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '1 years', $timezone = null),
		'fecha_hasta'          => $faker->dateTimeBetween($startDate = 'now', $endDate = '3 years', $timezone = null),
		'descuento_porcentaje' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 20),
		'descuento_monto'      => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 20),
		'tipos_cupones_id'     => TipoCupon::all()->random()->id,
	];
});
