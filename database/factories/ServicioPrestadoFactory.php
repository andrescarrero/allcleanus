<?php

use App\Models\Api\v1\CalificacionServicio;
use App\Models\Api\v1\CalificacionTrabajador;
use App\Models\Api\v1\Cliente;
use App\Models\Api\v1\Cupon;
use App\Models\Api\v1\Descuento;
use App\Models\Api\v1\EstadoServicio;
use App\Models\Api\v1\PagoServicio;
use App\Models\Api\v1\PrecioServicio;
use App\Models\Api\v1\Servicio;
use App\Models\Api\v1\ServicioTrabajador;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\ServicioPrestado::class, function (Faker $faker) {

	$probabilidad = $faker->numberBetween($min = 1, $max = 10);

	$ini = $faker->dateTimeInInterval($startDate = 'now', $interval = '+ 1 days', $timezone = null);
	$fin = $faker->dateTimeInInterval($startDate = $ini, $interval = '+ 1 days', $timezone = null);

	$servicioTrabajador = ServicioTrabajador::all()->random()->id;
	$servicio           = ServicioTrabajador::find($servicioTrabajador)->servicios_id;
	$horas              = Servicio::find($servicio)->horas;
	$precioServicio     = PrecioServicio::all()->random();

	return [
		'observacion_servicio'           => $faker->sentence($nbWords = 10, $variableNbWords = true),
		'fecha_inicio'                   => $ini,
		'fecha_fin'                      => $fin,
		'hora_inicio'                    => $horas == 'S' ? $ini : null,
		'hora_fin'                       => $horas == 'S' ? $fin : null,
		'fecha_hora_inicio_cliente'      => $horas == 'S' ? $ini : null,
		'fecha_hora_fin_cliente'         => $horas == 'S' ? $fin : null,
		'tiempo_estimado_llegada'        => null,
		'cantidad'                       => $probabilidad == 1 ? $faker->numberBetween($min = 1, $max = 10) : null,
		'ubicacion'                      => "prueba",
		// 'duracion_servicio'              => null,
		'clientes_id'                    => Cliente::all()->random()->id,
		'estados_servicios_id'           => EstadoServicio::all()->random()->id,
		'pagos_servicios_id'             => PagoServicio::all()->random()->id,
		'calificaciones_trabajadores_id' => $probabilidad > 3 ? CalificacionTrabajador::all()->random()->id : null,
		'calificaciones_servicios_id'    => $probabilidad <= 3 ? CalificacionServicio::all()->random()->id : null,
		'cupones_id'                     => $probabilidad > 8 ? Cupon::all()->random()->id : null,
		'servicios_trabajadores_id'      => $servicioTrabajador,
		'descuentos_id'                  => Descuento::all()->random()->id,
		'total_neto'                     => $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 150),
		'total_desc_cupon'               => $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 150),
		'precios_servicios_id'           => $precioServicio->id,
	];
});
