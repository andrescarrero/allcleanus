<?php

use App\Models\Api\v1\TipoDescuento;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\Descuento::class, function (Faker $faker) {

	$probabilidadPorcentaje = $faker->numberBetween($min = 0, $max = 1);

	return [
		'clave'                => $faker->regexify('[0-9]{10}'),
		'cantidad_descuentos'  => $faker->numberBetween($min = 0, $max = 100),
		'fecha_desde'          => $faker->dateTimeBetween($startDate = '-1 years', $endDate = '1 years', $timezone = null),
		'fecha_hasta'          => $faker->dateTimeBetween($startDate = 'now', $endDate = '3 years', $timezone = null),
		'monto_minimo'         => $faker->numberBetween($min = 50, $max = 100),
		'monto_maximo'         => $faker->numberBetween($min = 100, $max = 150),
		'descuento_porcentaje' => $probabilidadPorcentaje == 1 ? $faker->numberBetween($min = 0, $max = 30) : null,
		'descuento_monto'      => $probabilidadPorcentaje != 1 ? $faker->numberBetween($min = 0, $max = 50) : null,
		'tipos_descuentos_id'  => TipoDescuento::all()->random()->id,
	];
});
