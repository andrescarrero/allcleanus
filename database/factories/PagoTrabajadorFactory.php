<?php

use App\Models\Api\v1\Trabajador;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\PagoTrabajador::class, function (Faker $faker) {
	return [
		'fecha_pago'      => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
		'monto_pago'      => $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 150),
		'estado'          => $faker->randomElement($array = array('En espera', 'Pagado')),
		'trabajadores_id' => Trabajador::all()->random()->id,
	];
});
