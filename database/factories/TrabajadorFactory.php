<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\Trabajador::class, function (Faker $faker) {

	// $faker = new \Faker\Generator();
	$probabilidadActivo = $faker->numberBetween($min = 1, $max = 10);
	$probabilidadRetiro = $faker->numberBetween($min = 1, $max = 10);
	$correo             = $faker->unique()->safeEmail;
	$faker->addProvider(new \Faker\Provider\es_VE\Person($faker));

	return [
		'identificacion'  => $faker->nationalId,
		'tipodocumento'   => $faker->randomElement($array = array('E', 'V', 'P')),
		'nombre'          => $faker->firstName,
		'apellido'        => $faker->lastName,
		'correo'          => $correo,
		'contrasena'      => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret,
		'cuenta_bancaria' => $faker->iban(),
		'condicion'       => $probabilidadActivo > 2 ? 'A' : 'I',
		'fecha_ingreso'   => $faker->dateTimeBetween($startDate = '-5 years', $endDate = '-2 years', $timezone = null),
		'fecha_retiro'    => $probabilidadRetiro > 2 ? null : $faker->dateTimeBetween($startDate = '-1 year', $endDate = 'now', $timezone = null),
		'token'           => str_random(45),
		// 'foto'            => $faker->imageUrl($width = 640, $height = 480),
		'foto'            => $faker->imageUrl($width = 640, $height = 480),
		'monto_acumulado' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 500),
		'telefono'        => $faker->phoneNumber,
		'telefono_2'      => $faker->phoneNumber,
	];

});
