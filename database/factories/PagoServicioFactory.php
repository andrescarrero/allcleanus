<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\PagoServicio::class, function (Faker $faker) {

	$probabilidad = $faker->numberBetween($min = 1, $max = 15);

	return [
		'pago'       => $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 150),

		'devolucion' => $probabilidad > 1 && $probabilidad < 5 ? $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 150) : null,

		'recargo'    => $probabilidad > 5 && $probabilidad < 10 ? $faker->randomFloat($nbMaxDecimals = 2, $min = 50, $max = 150) : null,

		'estado'     => $faker->randomElement($array = array('To pay', 'Paid Out', '
In process', )),
	];
});
