<?php

use App\Models\Api\v1\Servicio;
use App\Models\Api\v1\Trabajador;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\ServicioTrabajador::class, function (Faker $faker) {
	return [
		'trabajadores_id' => Trabajador::all()->random()->id,
		'servicios_id'    => Servicio::all()->random()->id,
	];
});
