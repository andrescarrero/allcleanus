<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\Cliente::class, function (Faker $faker) {

	$probabilidadAdmin = $faker->numberBetween($min = 1, $max = 10);
	$faker->addProvider(new \Faker\Provider\es_VE\Person($faker));

	return [
		'identificacion'  => $faker->nationalId,
		'tipodocumento'   => $faker->randomElement($array = array('E', 'V', 'P')),
		'nombre'          => $faker->firstName,
		'apellido'        => $faker->lastName,
		// 'correo'          => $faker->unique()->safeEmail,
		// 'contrasena'      => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret,,
		'ubicacion'       => "prueba",
		// 'token'           => str_random(45),
		'cuenta_bancaria' => $faker->iban(),
		'telefono'        => $faker->phoneNumber,
		'telefono_2'      => $faker->phoneNumber,
		'fecha_registro'  => $faker->dateTimeBetween($startDate = '-5 years', $endDate = '-1 years', $timezone = null),
		'roles_id'        => $probabilidadAdmin == 1 ? 1 : 2,
		'users_id'        => User::all()->random()->id,
	];
});
