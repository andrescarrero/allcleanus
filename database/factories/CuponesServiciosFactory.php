<?php

use App\Models\Api\v1\Cupon;
use App\Models\Api\v1\Servicio;
use App\Models\Api\v1\TipoServicio;
use Faker\Generator as Faker;

$factory->define(App\Models\Api\v1\CuponServicio::class, function (Faker $faker) {

	$probabilidadServicio = $faker->numberBetween($min = 0, $max = 1);

	return [
		'cupones_id'         => Cupon::select('cupones.id')->join('tipos_cupones', 'cupones.tipos_cupones_id', '=', 'tipos_cupones.id')->where('tipos_cupones.id', '<>', 'G')->inRandomOrder()->first(),
		'tipos_servicios_id' => $probabilidadServicio == 1 ? TipoServicio::all()->random()->id : null,
		'servicios_id'       => $probabilidadServicio != 1 ? Servicio::all()->random()->id : null,
	];
});
