<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreciosServiciosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('precios_servicios', function (Blueprint $table) {
			$table->increments('id');
			$table->float('precio')->nullable();
			$table->integer('atributos_id')->unsigned()->nullable();
			$table->integer('especificaciones_id')->unsigned()->nullable();
			$table->integer('servicios_id')->unsigned();
			$table->integer('precios_servicios_id')->unsigned()->nullable();

			$table->foreign('atributos_id')->references('id')->on('atributos');
			$table->foreign('especificaciones_id')->references('id')->on('especificaciones');
			$table->foreign('servicios_id')->references('id')->on('servicios');
			$table->foreign('precios_servicios_id')->references('id')->on('precios_servicios');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('precios_servicios');
	}
}
