<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'servicios';

	/**
	 * Run the migrations.
	 * @table servicios
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('servicio', 45);
			$table->float('porc_trabajador');
			$table->float('porc_tercero');
			// $table->string('horas', 1);
			// $table->float('costo');
			$table->integer('tipos_servicios_id')->unsigned();

			$table->foreign('tipos_servicios_id')->references('id')->on('tipos_servicios');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
