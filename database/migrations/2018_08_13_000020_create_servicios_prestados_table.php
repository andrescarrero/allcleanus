<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosPrestadosTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'servicios_prestados';

	/**
	 * Run the migrations.
	 * @table servicios_prestados
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('observacion_servicio')->nullable();
			$table->date('fecha_inicio');
			$table->date('fecha_fin')->nullable();
			$table->time('hora_inicio')->nullable();
			$table->time('hora_fin')->nullable();
			$table->dateTime('fecha_hora_inicio_cliente')->nullable();
			$table->dateTime('fecha_hora_fin_cliente')->nullable();
			$table->time('tiempo_estimado_llegada')->nullable();
			// $table->time('duracion_servicio')->nullable();
			$table->float('cantidad')->nullable();
			$table->string('ubicacion');
			$table->integer('clientes_id')->unsigned();
			$table->integer('estados_servicios_id')->unsigned();
			$table->integer('pagos_servicios_id')->unsigned()->nullable();
			$table->integer('calificaciones_trabajadores_id')->unsigned()->nullable();
			$table->integer('calificaciones_servicios_id')->unsigned()->nullable();
			$table->integer('cupones_id')->unsigned()->nullable();
			$table->integer('servicios_trabajadores_id')->unsigned();
			$table->integer('descuentos_id')->unsigned()->nullable();
			//new 29/1/2019
			$table->float('total_neto');
            $table->float('total_desc_cupon');
            $table->integer('precios_servicios_id')->unsigned();

			$table->foreign('clientes_id')->references('id')->on('clientes');

			$table->foreign('estados_servicios_id')->references('id')->on('estados_servicios');

			$table->foreign('pagos_servicios_id')->references('id')->on('pagos_servicios');

			$table->foreign('calificaciones_trabajadores_id')->references('id')->on('calificaciones_trabajadores');

			$table->foreign('calificaciones_servicios_id')->references('id')->on('calificaciones_servicios');

			$table->foreign('cupones_id')->references('id')->on('cupones');

			$table->foreign('servicios_trabajadores_id')->references('id')->on('servicios_trabajadores');

			$table->foreign('descuentos_id')->references('id')->on('descuentos');

			$table->foreign('precios_servicios_id')->references('id')->on('precios_servicios');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
