<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagosTrabajadoresTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'pagos_trabajadores';

	/**
	 * Run the migrations.
	 * @table pagos_trabajadores
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->dateTime('fecha_pago');
			$table->float('monto_pago');
			$table->string('estado', 15)->nullable();
			$table->integer('trabajadores_id')->unsigned();

			$table->foreign('trabajadores_id')->references('id')->on('trabajadores');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
