<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposCuponesTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'tipos_cupones';

	/**
	 * Run the migrations.
	 * @table tipos_cupones
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('tipo_cupon', 1);
			$table->string('descripcion', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
