<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrabajadoresTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'trabajadores';

	/**
	 * Run the migrations.
	 * @table trabajadores
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('identificacion', 13)->unique();
			$table->string('tipodocumento', 1);
			$table->string('nombre', 45);
			$table->string('apellido', 45);
			$table->string('correo', 45)->unique();
			$table->string('contrasena');
			$table->string('cuenta_bancaria', 45)->nullable();
			$table->string('condicion', 1);
			$table->dateTime('fecha_ingreso');
			$table->dateTime('fecha_retiro')->nullable();
			$table->string('token', 45);
			$table->string('foto', 45)->nullable();
			$table->float('monto_acumulado');
			$table->string('telefono', 45)->nullable();
			$table->string('telefono_2', 45)->nullable();
			$table->integer('empresas_id')->unsigned()->nullable();

			$table->foreign('empresas_id')->references('id')->on('empresas');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
