<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosTrabajadoresTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'servicios_trabajadores';

	/**
	 * Run the migrations.
	 * @table servicios_trabajadores
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->integer('trabajadores_id')->unsigned();
			$table->integer('servicios_id')->unsigned();

			$table->foreign('trabajadores_id')->references('id')->on('trabajadores');

			$table->foreign('servicios_id')->references('id')->on('servicios');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
