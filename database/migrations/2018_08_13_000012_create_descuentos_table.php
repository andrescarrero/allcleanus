<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescuentosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('descuentos', function (Blueprint $table) {
			$table->increments('id');
			$table->string('clave', 10);
			$table->integer('cantidad_descuentos');
			$table->date('fecha_desde');
			$table->date('fecha_hasta')->nullable();
			$table->float('monto_minimo')->nullable();
			$table->float('monto_maximo')->nullable();
			$table->float('descuento_porcentaje')->nullable();
			$table->float('descuento_monto')->nullable();
			$table->integer('tipos_descuentos_id')->unsigned();

			$table->foreign('tipos_descuentos_id')->references('id')->on('tipos_descuentos');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('descuentos');
	}
}
