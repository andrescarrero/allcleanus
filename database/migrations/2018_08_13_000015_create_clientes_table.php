<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'clientes';

	/**
	 * Run the migrations.
	 * @table clientes
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('tipodocumento', 1);
			$table->string('identificacion', 13);
			$table->string('nombre', 45);
			$table->string('apellido', 45);
			// $table->string('correo', 45)->unique();
			// $table->string('contrasena');
			$table->string('ubicacion', 45);
			// $table->string('token', 45)->nullable();
			$table->string('cuenta_bancaria', 45)->nullable();
			$table->string('telefono', 45)->nullable();
			$table->string('telefono_2', 45)->nullable();
			$table->dateTime('fecha_registro')->nullable();
			$table->integer('roles_id')->unsigned();
			$table->integer('users_id')->unsigned();

			$table->foreign('roles_id')->references('id')->on('roles');
			$table->foreign('users_id')->references('id')->on('users');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
