<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuponesTable extends Migration {
	/**
	 * Schema table name to migrate
	 * @var string
	 */
	public $set_schema_table = 'cupones';

	/**
	 * Run the migrations.
	 * @table cupones
	 *
	 * @return void
	 */
	public function up() {
		Schema::create($this->set_schema_table, function (Blueprint $table) {
			$table->increments('id');
			$table->string('clave', 10)->unique();
			$table->integer('cantidad_cupones');
			$table->date('fecha_desde');
			$table->date('fecha_hasta')->nullable();
			$table->float('descuento_porcentaje')->nullable();
			$table->float('descuento_monto')->nullable();
			$table->integer('tipos_cupones_id')->unsigned();

			$table->foreign('tipos_cupones_id')->references('id')->on('tipos_cupones');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists($this->set_schema_table);
	}
}
