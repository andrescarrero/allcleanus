<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromocionesServiciosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('promociones_servicios', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('cupones_id')->unsigned()->nullable();
			$table->integer('tipos_servicios_id')->unsigned()->nullable();
			$table->integer('servicios_id')->unsigned()->nullable();
			$table->integer('descuentos_id')->unsigned()->nullable();

			$table->foreign('cupones_id')->references('id')->on('cupones');

			$table->foreign('tipos_servicios_id')->references('id')->on('tipos_servicios');

			$table->foreign('servicios_id')->references('id')->on('servicios');

			$table->foreign('descuentos_id')->references('id')->on('descuentos');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('promociones_servicios');
	}
}
